package main

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"os"
	"strconv"
)

func regularQuery(MOUCpostgresqlURL string, moucMin int64, moucMax int64, query string) {
	conn, err := pgx.Connect(context.Background(), MOUCpostgresqlURL)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect: %v\n", err)
		os.Exit(1)
	}

	for i, val := moucMin, moucMax; i < val; i++ { // 520101000000001, 520101999999999
		rows, err := conn.Query(context.Background(), query, i)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Query failed: %v\n", err)
			os.Exit(1)
		}

		// defer rows.Close()
		for rows.Next() {
			var drugTherapySchemeID int
			err = rows.Scan(&drugTherapySchemeID)
			if err != nil {
				os.Exit(1)
			}
			fmt.Printf("%d\t%d\n", i, drugTherapySchemeID)
		}
	}
}

func main() {
	MOUCpostgresqlURL := os.Args[1]
	moucMin, _ := strconv.ParseInt(os.Args[2], 10, 64)
	moucMax, _ := strconv.ParseInt(os.Args[3], 10, 64)
	moucDate := os.Args[4]
	queryNumber, _ := strconv.ParseInt(os.Args[5], 10, 32)
	query := make([]string, 2)
	query[0] = fmt.Sprintf(`
                                        select distinct
                                                mouc.DrugTherapyScheme_id as "DrugTherapyScheme_id"
                                        from
                                                v_MesOldUslugaComplex mouc
                                                inner join v_DrugTherapyScheme dts on dts.DrugTherapyScheme_id = mouc.DrugTherapyScheme_id
                            left join v_MesOld mo on mo.Mes_id = mouc.Mes_id
                                        where mouc.UslugaComplex_id = any(array(select eu.UslugaComplex_id from v_EvnUsluga eu where eu.EvnUsluga_pid = $1))
                                                and mouc.DrugTherapyScheme_id is not null
                            and (mo.MesType_id is null or mo.MesType_id = 10)
                                                and coalesce(dts.DrugTherapyScheme_endDate, '%s'::timestamp) >= '%s'::timestamp`, moucDate, moucDate)
	query[1] = fmt.Sprintf(`
                                        select distinct
                                                mouc.DrugTherapyScheme_id as "DrugTherapyScheme_id"
                                        from
                                                v_EvnUsluga eu
                                                inner join v_MesOldUslugaComplex mouc on mouc.UslugaComplex_id = eu.UslugaComplex_id
                                                inner join v_DrugTherapyScheme dts on dts.DrugTherapyScheme_id = mouc.DrugTherapyScheme_id
                            left join v_MesOld mo on mo.Mes_id = mouc.Mes_id
                                        where eu.EvnUsluga_pid = $1
                                                and mouc.DrugTherapyScheme_id is not null
                            and (mo.MesType_id is null or mo.MesType_id = 10)
                                                and coalesce(dts.DrugTherapyScheme_endDate, '%s'::timestamp) >= '%s'::timestamp`, moucDate, moucDate)
	regularQuery(MOUCpostgresqlURL, moucMin, moucMax, query[queryNumber])
}
